# frozen_string_literal: true

class AddOnlyForRegisteredInChecklists < ActiveRecord::Migration[5.2]
  def change
    add_column :checklists, :only_for_registered, :boolean, default: false
  end
end
