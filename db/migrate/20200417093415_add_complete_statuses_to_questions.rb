# frozen_string_literal: true

class AddCompleteStatusesToQuestions < ActiveRecord::Migration[5.2]
  def change
    add_column :questions, :completed, :boolean,  default: false
    add_column :questions, :next_complete_status, :string, default: 'Mark as complete'
  end
end
