# frozen_string_literal: true

# Controller for managing the Questions
class QuestionsController < ApplicationController
  before_action :set_question, :set_checklist, only: %i[switch_complete_status]

  def switch_complete_status
    QuestionService::UpdateCompleteStatus.call(@question)

    redirect_to checklist_path(@checklist),
                notice: 'Complete Status was successfully updated.'
  end

  private

  def set_question
    @question = Question.find(params[:id])
  end

  def set_checklist
    @checklist = Checklist.find(@question.checklist_id)
  end
end
