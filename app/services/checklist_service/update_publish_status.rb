# frozen_string_literal: true

# Services: Module ChecklistService and its Class UpdatePublishStatus
module ChecklistService
  # Class for updating Checklist publish status
  class UpdatePublishStatus < ApplicationService
    def initialize(checklist)
      @checklist = checklist
    end

    def call
      @checklist.published = !@checklist.published
      @status_ar = ['Mark as uncomplete', 'Mark as complete']

      @current_status = if @checklist.next_publish_status == 'Publish'
                          'Mark as complete'
                        elsif @checklist.next_publish_status == 'Unpublish'
                          'Mark as uncomplete'
                        else
                          @checklist.next_publish_status
      end

      @status_ar.delete(@current_status)
      @checklist.next_publish_status = @status_ar[0]
      @checklist.save
    end
  end
end
