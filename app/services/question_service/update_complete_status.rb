# frozen_string_literal: true

# Services: Module QuestionService and its Class UpdateCompleteStatus
module QuestionService
  # Class for updating Question publish status
  class UpdateCompleteStatus < ApplicationService
    def initialize(question)
      @question = question
    end

    def call
      @question.completed = !@question.completed
      @status_ar = ['Mark as uncomplete', 'Mark as complete']

      @status_ar.delete(@question.next_complete_status)
      @question.next_complete_status = @status_ar[0]
      @question.save
    end
  end
end
